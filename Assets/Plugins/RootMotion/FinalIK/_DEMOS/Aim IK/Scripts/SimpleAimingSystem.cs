﻿using RootMotion.Demos;
using UnityEngine;
using System.Collections;
using RootMotion.FinalIK;

namespace RootMotion.Demos {

	// Demonstrating 360-degree aiming system built with 6 static aiming poses and AimIK.
	public class SimpleAimingSystem : MonoBehaviour {
		
		[Tooltip("AimPoser is a tool that returns an animation name based on direction.")]
		public AimPoser aimPoser;
		
		[Tooltip("Reference to the AimIK component.")]
		public AimIK aim;
		
		[Tooltip("Reference to the LookAt component (only used for the head in this instance).")]
		public LookAtIK lookAt;
		
		[Tooltip("Reference to the Animator component.")]
		public Animator animator;
		
		[Tooltip("Time of cross-fading from pose to pose.")]
		public float crossfadeTime = 0.2f;

		[Tooltip("Will keep the aim target at a distance.")]
		public float minAimDistance = 0.5f;

		public Transform targetTransform;
		private AimPoser.Pose aimPose, lastPose;
		private bool shouldAim;

		void Start() {
			// Disable IK components to manage their updating order
			aim.enabled = false;
			lookAt.enabled = false;
			shouldAim = false;
			SetTarget (GameObject.Find("Aim Target").transform);
		}
		
		// LateUpdate is called once per frame
		void LateUpdate () {
			if (aim.solver.target == null) {
				//Debug.LogWarning("AimIK and LookAtIK need to have their 'Target' value assigned.", transform);
			}

			// Switch aim poses (Legacy animation)
			Pose();

			// Update IK solvers
			aim.solver.Update();
			if (lookAt != null) lookAt.solver.Update();
		}

		private void SetTarget(Transform target){

			if (target != null) {
				targetTransform = target;

			} else {
				targetTransform = null;
				aim.enabled = false;
			}
			aim.solver.target = target;
		

		}
		
		private void Pose() {

			//if (targetTransform!=null)
			if (targetTransform!=null) {
				// Make sure aiming target is not too close (might make the solver instable when the target is closer to the first bone than the last bone is).
				LimitAimTarget ();
				
				// Get the aiming direction
				Vector3 direction = (aim.solver.target.position - aim.solver.bones [0].transform.position);
				// Getting the direction relative to the root transform
				Vector3 localDirection = transform.InverseTransformDirection (direction);
				
				// Get the Pose from AimPoser
				aimPose = aimPoser.GetPose (localDirection);
				
				// If the Pose has changed
				if (aimPose != lastPose) {
					// Increase the angle buffer of the pose so we won't switch back too soon if the direction changes a bit
					aimPoser.SetPoseActive (aimPose);
					//Debug.Log (aimPose.name);
					
					// Store the pose so we know if it changes
				
				
				
					if (lastPose != null && (lastPose.name == "Aim Back" && aimPose.name == "Aim Left") || lastPose != null && (lastPose.name == "Aim Left" && aimPose.name == "Aim Back")|| lastPose != null && (lastPose.name == "Aim Forward" && aimPose.name == "Aim Right")) {
						//Debug.Log ("Disable AIM");
						shouldAim = false;
						//aim.solver.IKPositionWeight = 0.2f;	
					}
				
					lastPose = aimPose;
				
				} else {
					
				
				}
				
				// Direct blending
				foreach (AimPoser.Pose pose in aimPoser.poses) {
					if (pose == aimPose) {
						
						DirectCrossFade (pose.name, 1f);
					} else {
						DirectCrossFade (pose.name, 0f);
					}
				}
				
				if (shouldAim) {
					AimIKWeightFade (1f, crossfadeTime / 4);
				} else {
					AimIKWeightFade (0.25f, crossfadeTime / 2);
				}
			}
		}
		
		// Make sure aiming target is not too close (might make the solver instable when the target is closer to the first bone than the last bone is).
		void LimitAimTarget() {
			Vector3 aimFrom = aim.solver.bones[0].transform.position;
			Vector3 direction = (aim.solver.target.position - aimFrom);
			direction = direction.normalized * Mathf.Max(direction.magnitude, minAimDistance);
			
			aim.solver.target.position = aimFrom + direction;
		}
		
		// Uses Mecanim's Direct blend trees for cross-fading
		private void DirectCrossFade(string state, float target) {

			float fadeTime = crossfadeTime;

			if (lastPose.name == state) {
				//fadeTime=crossfadeTime/2;
			}
			float f = Mathf.MoveTowards(animator.GetFloat(state), target, Time.deltaTime * (1f / crossfadeTime));
			animator.SetFloat(state, f);

			//check if new pose is reached soon
			if (lastPose.name == state && f > 0.5 && !shouldAim) {

				shouldAim = true;
			



			}
			//Debug.Log ("state: " + state + " value: " + f);


		}

		private void AimIKWeightFade(float target, float time){
			aim.solver.IKPositionWeight = Mathf.Lerp (aim.solver.IKPositionWeight, target, Time.deltaTime * (1f / time));

		}
	}
}
