﻿using UnityEngine;
using System.Collections;



// The simplest multi-purpose locomotion controller for demo purposes. Can use root motion, simple procedural motion or the CharacterController
[RequireComponent(typeof(Animator))]
public class SimpleLocomotionWithAim : MonoBehaviour {

	// The character rotation mode
	[System.Serializable]
	public enum RotationMode {
		Smooth,
		Linear
	}




	[Tooltip("Acceleration of movement.")]
	[SerializeField] float accelerationTime = 0.2f;

	[Tooltip("Turning speed.")]
	[SerializeField] float turnTime = 0.2f; 

	[Tooltip("If true, will run on left shift, if not will walk on left shift.")]
	[SerializeField] bool walkByDefault = true;

	[Tooltip("Smooth or linear rotation.")]
	[SerializeField] RotationMode rotationMode;

	[Tooltip("Procedural motion speed (if not using root motion).")]
	[SerializeField] float moveSpeed = 3f;

	// Is the character grounded (using very simple y < something here for simplicity's sake)?
	public bool isGrounded { get; private set; }

	private RootMotion.FinalIK.AimIK aim;

	private Animator animator;
	private float speed;
	private float angleVel;
	private float speedVel;
	private Vector3 linearTargetDirection;
	private CharacterController characterController;

	public bool isAiming;

	private Vector3 lastInputVector;

	void Start() {


		animator = GetComponent<Animator>();
		characterController = GetComponent<CharacterController>();
		aim = GetComponent<RootMotion.FinalIK.AimIK> ();


	}

	void Update() {


		//Debug.Log ("Distance: " + newDistance + "walksBackwards:"+ walksBackwards);
		// Very basic planar method, should use collision events
		isGrounded = transform.position.y < 0.1f;
		Move();
		Rotate();

	}

	void LateUpdate() {


	}

	private void Rotate() {
		if (!isGrounded) return;

		// Updating the rotation of the character
		Vector3 inputVector = GetInputVector();
		if (inputVector == Vector3.zero) return;

		Vector3 forward = transform.forward;



		switch(rotationMode) {
		case RotationMode.Smooth:

			if (!isAiming) {




				Vector3 targetDirection = inputVector;

				float angleForward = Mathf.Atan2 (forward.x, forward.z) * Mathf.Rad2Deg;

				float angleTarget = Mathf.Atan2 (targetDirection.x, targetDirection.z) * Mathf.Rad2Deg;

				// Smoothly rotating the character

				float angle = Mathf.SmoothDampAngle (angleForward, angleTarget, ref angleVel, turnTime);


				transform.rotation = Quaternion.AngleAxis (angle, Vector3.up);
			
			} else {

				Vector3 dir = (aim.solver.target.transform.position - transform.position);


				float angleForward = Mathf.Atan2 (forward.x, forward.z) * Mathf.Rad2Deg;
				float angleTarget = Mathf.Atan2 (dir.x, dir.z) * Mathf.Rad2Deg;

				float angle = Mathf.SmoothDampAngle (angleForward, angleTarget, ref angleVel, turnTime);
				transform.rotation = Quaternion.AngleAxis (angle, Vector3.up);


			}



			break;
		case RotationMode.Linear:
			Vector3 inputVectorRaw = GetInputVectorRaw();
			if (inputVectorRaw != Vector3.zero) linearTargetDirection = inputVectorRaw;

			forward = Vector3.RotateTowards(forward, linearTargetDirection, Time.deltaTime * (1f /turnTime), 1f);
			forward.y = 0f;
			transform.rotation = Quaternion.LookRotation(forward);
			break;
		}
	}

	private void Move() {

		Vector3 inputVector=GetInputVector();

		float a = SignedAngle(new Vector3(inputVector.x, 0, inputVector.z), transform.forward);
		// Normalize the angle
		if (a < 0)
		{
			a *= -1;
		}
		else
		{
			a = 360 - a;
		}
		// Take into consideration the angle of the camera
		a += Camera.main.transform.eulerAngles.y;

		float aRad = Mathf.Deg2Rad*a; // degrees to radians

		if (inputVector.x != 0 || inputVector.z != 0)
		{
			inputVector = new Vector3(Mathf.Sin(aRad),0f, Mathf.Cos(aRad));
		}


		float xVelocity = 0f, yVelocity = 0f;
		float smoothTime = 0.05f;

		inputVector = new Vector3(Mathf.SmoothDamp(lastInputVector.x, inputVector.x, ref xVelocity, smoothTime),0f, Mathf.SmoothDamp(lastInputVector.z, inputVector.z, ref yVelocity, smoothTime));

		// Update the Animator with our values so that the blend tree updates

		inputVector=inputVector;
		animator.SetFloat("VelX", inputVector.x);
		animator.SetFloat("VelZ", inputVector.z);

		lastInputVector = inputVector;

		// Speed interpolation
		isAiming = Input.GetKey (KeyCode.LeftShift) ? false : true;

		animator.SetBool ("Aiming", isAiming);
		ConsoleProDebug.Watch("Aiming", isAiming.ToString());

		float speedTarget = walkByDefault? (Input.GetKey(KeyCode.LeftShift)? 1f: 0.5f): (Input.GetKey(KeyCode.LeftShift)? 0.5f: 1f);


		if (isAiming) {
			speedTarget = 0.5f;

		}
			
		speed = Mathf.SmoothDamp(speed, speedTarget, ref speedVel, accelerationTime);

		// Moving the character by root motion
		float s = GetInputVector().magnitude * speed;

		animator.SetFloat("Speed", s);

		ConsoleProDebug.Watch("Speed", s.ToString());

		// Procedural motion if we don't have root motion
		bool proceduralMotion = !animator.hasRootMotion && isGrounded;



		if (proceduralMotion&&!isAiming) {
			Vector3 move = transform.forward * s * moveSpeed;

			if (characterController != null) {
				characterController.SimpleMove(move);
			} else {
				transform.position += move * Time.deltaTime;
			}
		}

		if (false&&isAiming) {
			animator.SetFloat ("VelX", GetInputVector().x);
			animator.SetFloat ("VelZ", GetInputVector().z);

			//characterController.Move (GetInputVector ()*0.01f);
		}
	}

	// Reads the Input to get the movement direction.
	private Vector3 GetInputVector() {
		Vector3 d = new Vector3(
			Input.GetAxis("Horizontal"),
			0f,
			Input.GetAxis("Vertical")
		);

		d.z += Mathf.Abs(d.x) * 0.05f;
		d.x -= Mathf.Abs(d.z) * 0.05f;

		return d;
	}

	private Vector3 GetInputVectorRaw() {
		return new Vector3(
			Input.GetAxisRaw("Horizontal"),
			0f,
			Input.GetAxisRaw("Vertical")
		);
	}

	private float SignedAngle(Vector3 a, Vector3 b)
	{
		return Vector3.Angle(a, b) * Mathf.Sign(Vector3.Cross(a, b).y);
	}
}

